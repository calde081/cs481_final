import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';

Color generateColor() => Color(0xFFFFFFFF & Random().nextInt(0xFFFFFFFF));

IconData generateIcon() {
  Random random = new Random();
  final iconCollection = [Icons.favorite, Icons.audiotrack, Icons.beach_access, Icons.ac_unit, Icons.bug_report];
  return iconCollection[random.nextInt(5)];
}
class LoadingScreen extends StatefulWidget {
  @override
  _FadeTransitionDemoState createState() => _FadeTransitionDemoState();

}
class _FadeTransitionDemoState extends State<LoadingScreen> with SingleTickerProviderStateMixin {
  Color color;
  IconData icon;
  AnimationController _controller;
  Animation<double> _animation;
  CurvedAnimation _curve;
  double _progress = 0;
  double _opacity = 1.0;
  @override
  void initState() {
    super.initState();
    color = Colors.deepPurple;
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 3));
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _animation = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(_curve);
    _controller.forward(from: 0.0);
    startTimer();
  }
  @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  void change() {
    setState(() {
      color = generateColor();
      icon = generateIcon();
    });
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  void startTimer() {
    _progress = 0;
    Timer.periodic(
      Duration(milliseconds: 150),
          (Timer timer) =>
          setState(()
          {
              if (_progress == 1)
              {
                timer.cancel();
              } else if (_progress < 1) {
                _progress += 0.05;
              }
            },
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color:Colors.grey),
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child:Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RotationTransition(
                turns: Tween(begin: 0.0, end: 1.0).animate(_animation),
                child: Icon(
                  Icons.data_usage_rounded,
                  color: Colors.deepPurple,
                  size: 100,
                ),
              ),
              LinearProgressIndicator(
                backgroundColor: Colors.white70,
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepPurple),
                value: _progress,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

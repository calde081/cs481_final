class Options{
  static const String share = "Share";
  static const String info = "Album Info";

  static const List<String> choices = <String>[
    info,
    share,
  ];
}
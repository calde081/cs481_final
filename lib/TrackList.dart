
import 'package:flutter/material.dart';
import 'Album.dart';
import 'Player.dart';
import 'Song.dart';
import 'Heart.dart';


class TrackList extends StatefulWidget {
  final Album album;
  TrackList({Key key, @required this.album}) : super(key: key);

  @override
  _TrackListState createState() => _TrackListState();
}

class _TrackListState extends State<TrackList>{
  bool shuffle = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 38, 50, 56),
          title: Center(child: Text(widget.album.artist + " - " + widget.album.title)),
      ),
      body:  Container(
        decoration: BoxDecoration(color:Colors.white),
        child:ListView(
          children: [Container (
            child: DataTable(columnSpacing: 35, columns: const <DataColumn>[
              DataColumn(
                label: Text(
                  'No',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Title',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Length',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Favorite',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  '',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ],
              rows:
              widget.album.songList.map(
                ((Song element) => DataRow(
                  cells: <DataCell>[
                    DataCell(Text(element.no)), //Extracting from Map element the value
                    DataCell(Text(element.title)),
                    DataCell(Text(element.length)),
                    DataCell(
                      Heart(song: element),
                    ),
                    DataCell(ActionChip(label: Icon(Icons.play_arrow,color: Colors.white,), onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Player(song: element, album: null, shuffle: false, listOfSongs: [],),
                        ),
                      );
                    }, backgroundColor: Color.fromARGB(255, 38, 50, 56),))
                  ],
                )),
              ).toList(),
            ),
          ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: 25,
                  width: 150,
                  child: RaisedButton(
                    color: Color.fromARGB(255, 38, 50, 56),
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Player(song: null, album: widget.album, shuffle: !shuffle, listOfSongs: [],),
                        ),
                      );
                    },
                    child: Text("Play All",style:TextStyle(color:Colors.white)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                  width: 150,
                  child: RaisedButton(
                    color: Color.fromARGB(255, 38, 50, 56),
                    // textColor: Colors.black,
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Player(song: null, album: widget.album, shuffle: shuffle, listOfSongs: [],),
                        ),
                      );
                    },
                    child: Text("Shuffle",style: TextStyle(color: Colors.white),),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
              ],
            ),
            Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
          ],
        )
      )
    );
  }
  Widget get _space => const SizedBox(height: 5);
}
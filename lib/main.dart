import 'package:flutter/material.dart';
import 'PrimaryPage.dart';
import 'FavoriteList.dart';
import 'Explore.dart';
import 'Login.dart';
var username = "mike";
void main()=>
    runApp(MaterialApp(
      routes: {
        '/': (context) => Login(),
        '/PrimaryPage': (context) => PrimaryPage(),
        '/FavoriteList': (context) => FavoriteList(),
        '/Explore': (context) => Explore(),
        '/MyBottomNavigationBar' : (context) => MyBottomNavigationBar(),
      },
      //home: MyBottomNavigationBar(),
      // localizationsDelegates: [
      //   S.delegate,
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      //   GlobalCupertinoLocalizations.delegate,
      // ],
      // supportedLocales: S.delegate.supportedLocales,
    ));
class MyBottomNavigationBar extends StatefulWidget {
  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _selectedIndex = 0;
  final List<Widget> _children =
  [
    PrimaryPage(),
    FavoriteList(),
    Explore(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    // if(_isPlayerReady) {
    //   _controller.load(_ids[index]);
    // }
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body:_children[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromARGB(255, 79,91,98),
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: IconButton(icon: Icon(Icons.album_sharp),
            ),
            title: Text('Album'),
          ),
          BottomNavigationBarItem(
            icon: IconButton(icon: Icon(Icons.favorite),
              // onPressed: (){
              //   Navigator.pushNamed(context, '/FavoriteList');
              // },
            ),
            //title: Text('Favorites', style: TextStyle(color: Colors.grey),),
            title: Text('Favorites'),
          ),
          BottomNavigationBarItem(
            icon: IconButton(icon: Icon(Icons.explore),
              // onPressed: (){
              //   Navigator.pushNamed(context, '/SettingsPage');
              // },
            ),
            title: Text('Explore'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}

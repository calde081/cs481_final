import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'Album.dart';
import 'Song.dart';
import 'dart:async';

/// Creates [YoutubePlayerDemoApp] widget.
class Player extends StatelessWidget {
  final Song song;
  final Album album;
  final bool shuffle;
  final List<Song> listOfSongs;

  Player({Key key, @required this.song, this.album, this.shuffle, this.listOfSongs}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MyHomePage(song: song, album: album, shuffle: shuffle, listOfSongs: listOfSongs);
  }
}
/// Homepage
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.song, this.album, this.shuffle, this.listOfSongs}) : super(key: key);
  final List<Song> listOfSongs;
  final bool shuffle;
  final Song song;
  final Album album;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  YoutubePlayerController _controller;
  TextEditingController _idController;
  TextEditingController _seekToController;

  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  double _volume = 100;
  bool _muted = false;
  bool _isPlayerReady = false;
  int position;
  String _lengthStr;
  int length;
  bool refreshed = false;
  IconData icon;
  Duration durPosition;
  int numTimesToPop = 1;
  String title;
  bool multipleSongs = false;
  bool once = false;
  bool loop = false;

  String songUrl;
  List<Song> _songs;
  int songNumber = 0;
  var strArray;

  @override
  void initState() {
    if(widget.song == null && widget.album != null) {
      _songs = List.from(widget.album.songList);
      multipleSongs = true;
    } // makes a deep copy of the songList rather than just pointing to the original list
    else if (widget.album == null && widget.listOfSongs.isNotEmpty){ // presumes that there's a list
      _songs = List.from(widget.listOfSongs);
      multipleSongs = true;
    }

    // grabbed randomized code from https://stackoverflow.com/questions/13554129/list-shuffle-in-dart
    if(widget.song == null && widget.shuffle) {
      var random = new Random();
      for (int i = _songs.length - 1; i > 0; i--) {
        var rnd = random.nextInt(i + 1);
        var temp = _songs[i];
        _songs[i] = _songs[rnd];
        _songs[rnd] = temp;
      }
    }

    // _songs is now randomized
    if (widget.song != null){
    _lengthStr = widget.song.length;
    songUrl = widget.song.url;
    title = widget.song.title;
    }  // initialize these variables to the first song in _songs
    else if(widget.song == null){
      _lengthStr = _songs[0].length;
      songUrl = _songs[0].url;
      title = _songs[0].title;
    }


    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: songUrl,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true

        ),
    )..addListener(listener);
    _idController = TextEditingController();
    _seekToController = TextEditingController();
    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;

    durPosition = _controller.value.position;
  }

  void listener() {
    position = _controller.value.position.inSeconds;
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }

      if(position > length - 2){ // listens for the end of the song, need to go onto next song automatically.

        // !multipleSongs implies that the request is to only play one song
        // or if it is multipleSongs, then check to see if it is the last song else load the next song.
        if(!multipleSongs || ( widget.song == null && songNumber == _songs.length)) {
          if(!loop){
            _controller.pause();
            refreshed = true;  // code will be altered below to start at beginning of entire list since this is associated with the refresh icon
          }
          else if(loop){
            if(!multipleSongs){
              _controller.seekTo(Duration.zero);
            }
            else if(multipleSongs){
              songNumber = 0;  // reset song number to start
              // load the next song in _songs
              print("song number: " + songNumber.toString());
              songUrl = _songs[songNumber].url;
              title = _songs[songNumber].title;
              _lengthStr = _songs[songNumber].length;
              _controller.load(_songs[songNumber].url);
              strArray = _lengthStr.split(":");
              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
              once = true;
              waiting();  // absolutely need this line !!! otherwise listener is called too quickly and skips songs by ++songNumber too many times
            }
          }
        }
        else if (_controller.value.hasPlayed && !once){ // just load the next song if multipleSongs and not at end

            ++songNumber; // increase songCount to next song

            // load the next song in _songs
            print("song number: " + songNumber.toString());
            songUrl = _songs[songNumber].url;
            title = _songs[songNumber].title;
            _lengthStr = _songs[songNumber].length;
            _controller.load(_songs[songNumber].url);
            strArray = _lengthStr.split(":");
            length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
            once = true;
            waiting();  // absolutely need this line !!! otherwise listener is called too quickly and skips songs by ++songNumber too many times
        }
        setState(() {
          icon = chooseIcon();
        });
      }
      else{
        icon = chooseIcon();  // if not at the end of the song
      }
  }

  Future<void> waiting() async{
    await Future.delayed(Duration(seconds: 10));
    once = false;
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    _idController.dispose();
    _seekToController.dispose();
    super.dispose();
  }

  IconData chooseIcon(){
    if (refreshed){
      return Icons.cached_rounded;
    }
    else if (!refreshed && _controller.value.isPlaying) {
      //_controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
      return Icons.pause;
    }
    else if (!refreshed && !_controller.value.isPlaying){
      return Icons.play_arrow;
    }
  }


  @override
  Widget build(BuildContext context) {
    strArray = _lengthStr.split(":");
    length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);

    return YoutubePlayerBuilder(
      onExitFullScreen: () {
        // The player forces portraitUp after exiting fullscreen. This overrides the behaviour.
        SystemChrome.setPreferredOrientations(DeviceOrientation.values);
      },
      player: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        progressIndicatorColor: Color.fromARGB(255, 38, 50, 56),
        topActions: <Widget>[
          const SizedBox(width: 8.0),
          Expanded(
            child: Text(
              _controller.metadata.title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 18.0,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          ),
        ],
        onReady: () {
          _isPlayerReady = true;
          print("checking");
        },
        // this property doesn't work very well, the video will just continue loading rather than go onto the next video if _controller.load() is used
        onEnded: (data){
          print("song ended");
        },
      ),
      builder: (context, player) => Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 38, 50, 56),
          title: Center(child: Text(title)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          color: Colors.white,
          child: ListView(
            children: [
              player,
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                 // _space,
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(flex: 5, child: _text('Title', _videoMetaData.title)),
                        Expanded(
                          flex: 1,
                          child: IconButton(
                            icon: Icon(_muted ? Icons.volume_off : Icons.volume_up),
                            onPressed: _isPlayerReady
                                ? () {
                              _muted
                                  ? _controller.unMute()
                                  : _controller.mute();
                              setState(() {
                                _muted = !_muted;
                              });
                            }
                                : null,
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: IconButton(
                            icon: Icon(
                              Icons.all_inclusive_rounded,
                              color: loop ? Colors.black : Colors.grey[500],
                              // _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                            ),
                            onPressed: _isPlayerReady ? () {
                              setState(() {
                                loop = !loop;
                              });
                            }
                                : null,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      _text('Channel', _videoMetaData.author),
                    ],
                  ),
                  _space,
                  Row(
                    children: [
                      _text(
                        'Playback Quality',
                        _controller.value.playbackQuality,
                      ),
                      const Spacer(),
                      _text(
                        'Playback Rate',
                        '${_controller.value.playbackRate}x  ',
                      ),
                    ],
                  ),
                  _space,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IconButton(
                        icon: Icon(
                          icon,
                        ),
                        onPressed: _isPlayerReady ? () {
                          _controller.value.isPlaying ? _controller.pause() : _controller.play();
                          setState(() {
                            if(icon == Icons.cached_rounded){
                              refreshed = false;
                              if(multipleSongs){
                                songNumber = 0;
                                songUrl = _songs[songNumber].url;
                                title = _songs[songNumber].title;
                                _lengthStr = _songs[songNumber].length;
                                _controller.load(_songs[songNumber].url);
                                strArray = _lengthStr.split(":");
                                length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                                once = true;
                                waiting();
                              }
                              else {
                                _controller.seekTo(durPosition);
                              }
                            }
                          });
                        }
                            : null,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.refresh,
                          // _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                        ),
                        onPressed: _isPlayerReady ? () {
                          _controller.value.isPlaying ? _controller.pause() : _controller.play();
                          setState(() {
                                _controller.seekTo(durPosition);
                                refreshed = false;
                          });
                        }
                            : null,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_outlined,
                          // _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                        ),
                        onPressed:  () {
                          print("song number: $songNumber");
                          // if !loop && !multipleSongs then nothing happens on forward or back
                          if(!loop && multipleSongs){
                            if(songNumber > 0){
                              --songNumber;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                          }
                          else if(loop && multipleSongs){
                            print("song number : $songNumber");
                            if(songNumber > 0){
                              --songNumber;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                            else if (songNumber == 0){
                              songNumber = _songs.length - 1;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                          }
                          setState(() {

                          });
                        }
                            //: null,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.arrow_forward_outlined,
                          // _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                        ),
                        onPressed: _isPlayerReady ? () {
                          // if !loop && !multipleSongs then nothing happens on forward or back
                          if(!loop && multipleSongs){
                            if(songNumber < _songs.length - 1){
                              ++songNumber;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                          }
                          else if(loop && multipleSongs){
                            if(songNumber < _songs.length - 1){
                              ++songNumber;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                            else if (songNumber == _songs.length - 1){
                              songNumber = 0;
                              songUrl = _songs[songNumber].url;
                              title = _songs[songNumber].title;
                              _lengthStr = _songs[songNumber].length;
                              _controller.load(_songs[songNumber].url);
                              strArray = _lengthStr.split(":");
                              length = (int.parse(strArray[0]) * 60) + int.parse(strArray[1]);
                              once = true;
                              waiting();
                            }
                          }
                          setState(() {

                          });
                        }
                            : null,
                      ),
                    ],
                  ),
                  _space,
                  Row(
                    children: <Widget>[
                      const Text(
                        "Volume",
                        style: TextStyle(fontWeight: FontWeight.w300),
                      ),
                      Expanded(
                        child: Slider(
                          inactiveColor: Colors.white,
                          activeColor: Color.fromARGB(255, 38, 50, 56),
                          value: _volume,
                          min: 0.0,
                          max: 100.0,
                          divisions: 10,
                          label: '${(_volume).round()}',
                          onChanged: _isPlayerReady
                              ? (value) {
                            setState(() {
                              _volume = value;
                            });
                            _controller.setVolume(_volume.round());
                          }
                              : null,
                        ),
                      ),
                    ],
                  ),
                  _space,
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget _text(String title, String value) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
      child: RichText(
        text: TextSpan(
          text: '$title : ',
          style: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          children: [
            TextSpan(
              text: value ?? '',
              style: const TextStyle(
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget get _space => const SizedBox(height: 5);
}

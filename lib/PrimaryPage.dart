import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inter_dialo_localization/Album.dart';
import 'package:inter_dialo_localization/TrackList.dart';
import 'package:inter_dialo_localization/main.dart';
import 'LoadingScreen.dart';
import 'Options.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:email_launcher/email_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

import 'User.dart';

class PrimaryPage extends StatefulWidget {
  @override
  _PrimaryPageState createState() => _PrimaryPageState();
}

class _PrimaryPageState extends State<PrimaryPage> {
  List<Album> albums = new List<Album>();
  int index;
  int _selectedIndex = 0;

  Future<List<Album>> fetchAlbum() async {
    final response = await http.get('http://3.21.129.217:3000/api/user?id=' + username);
    if (response.statusCode == 200) {
      var user = User.fromJson(json.decode(response.body));
      albums = user.albums;
      return albums;
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<List<Album>>  futureAlbum;
  @override
  void initState() {
    super.initState();
    futureAlbum = fetchAlbum();
  }

  void popUpOptions(String option){
    int index = 0;
    List<String> a = option.split(":");

    switch(a[1]){
      case "Suddenly": index = 0; break;
      case "Good Faith" : index = 1; break;
      case "21": index = 2; break;
      case "Yellow Submarine": index = 3; break;
      case "Fleetwood Mac": index = 4; break;
      case "Draw the Line": index = 5; break;
    }

    if(a[0] == Options.share){
      showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context){
            return Container(
              height: 250,
              color:Colors.grey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children:<Widget> [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:[
                      Text(
                        '   Share',
                        style: TextStyle(
                          // fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                      IconButton(
                        icon: const Icon(
                            Icons.close
                        ),
                        onPressed: () => Navigator.pop(context),
                      ) ,
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            icon: const Icon(
                                Icons.mail,
                                size: 40,
                            ),
                            onPressed: () async {
                              Email email = Email(
                                  to:[],
                                  cc:[],
                                  bcc:[],
                                  subject: 'Shared music',
                                  body: 'Check it out: Album ' + albums[index].title + ' by ' + albums[index].artist
                              );
                              await EmailLauncher.launch(email);
                            },

                          ),
                          Text("   Email",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          IconButton(
                            icon: const Icon(
                                Icons.message,
                                size: 40,
                                // color: Colors.black87,
                            ),
                            onPressed: () async {
                              String url = 'sms:?body=Check%20it%20out:%20' + albums[index].title + '%20by%20' + albums[index].artist ;
                              await launch(url);
                            },
                          ),
                          Text("  Message",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
      );
    }
    if(a[0] == Options.info){

      showDialog(context: context,
        child: new CupertinoAlertDialog(
          title: Text("Album Information\n"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Table(
                  children: <TableRow>[
                    TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Text("Album Title: ", textAlign: TextAlign.right,),
                          ),
                          Text(albums[index].title, textAlign: TextAlign.left),
                        ]
                    ),

                    TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Text("Artist: ", textAlign: TextAlign.right,),
                          ),
                          Text(albums[index].artist, textAlign: TextAlign.left,),
                        ]
                    ),
                    TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Text("Year Released: ", textAlign: TextAlign.right,),
                          ),
                          Text(albums[index].yearProduced, textAlign: TextAlign.left,),
                        ]
                    ),

                  ],

                ),
                // Text( +  +  + , textAlign: TextAlign.left,),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Dismiss"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }
  }
  Widget _buildTile(Album album) =>  Card(
    elevation: 0,
    color:Colors.transparent,
    child: Container(
      decoration: BoxDecoration(color: Color.fromARGB(255, 38,50,56)),
      child:Card(
        elevation: 0,
        color: Colors.transparent,
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Future.delayed(const Duration(milliseconds: 3200), () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TrackList(album: album),
                ),
              ) .then((value) {
                futureAlbum = fetchAlbum();
                setState(() {
                  _selectedIndex = _selectedIndex;
                });
                // you can do what you need here
                // setState etc.
              });
            });
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) => _buildAboutDialog(context, album),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(4),
            width: 300,
            height: 200,
            child: Column(
              children: [
                Stack(
                  children: [
                    Image.asset('assets/images/' + album.image),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        PopupMenuButton(
                          icon: new Icon(Icons.more_vert, color: Colors.black,),
                          color: Colors.grey,
                          onSelected: popUpOptions,
                          itemBuilder: (BuildContext context){
                            return Options.choices.map((String option){
                              return PopupMenuItem<String>(
                                //value: album.title,
                                value: option + ":" + album.title,
                                child: Text(option, style: TextStyle(color: Colors.white),),
                              );
                            }).toList();
                          },
                        ),
                      ],
                    ),
                    //IconButton(icon: Icon(Icons.),),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ),
    );

  Widget GetItemWidgets(List<Album> albums)
  {
    return new GridView.count(
        crossAxisCount: 1,
        crossAxisSpacing: 0.0,
        mainAxisSpacing: 2.0,
        children: albums.map((album) => _buildTile(album)).toList()
    );
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    // if(_isPlayerReady) {
    //   _controller.load(_ids[index]);
    // }
  }


  Widget _buildAboutDialog(BuildContext context, Album album) {
    Future.delayed(const Duration(milliseconds: 3000), () {
      Navigator.of(context).pop();
    });

    return new AlertDialog(
      backgroundColor: Colors.grey,
      content: Container(
        height: 250,
        child: LoadingScreen()
      )
    );

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 38,50,56),
        title: Center(child: const Text('My Muse', style: TextStyle(fontStyle: FontStyle.italic),)),
      ),
      // body: GetItemWidgets(albums),
      body: Center(
        child: Container(
          decoration: new BoxDecoration(color: Color.fromARGB(255, 38,50,56)),
          child:FutureBuilder<List<Album>>(
              future: futureAlbum,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return GetItemWidgets(albums);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              }
          ),
        )
      ),
    );
  }
}
// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

import 'Album.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.albums,
    this.id,
    this.v,
    this.image
  });

  List<Album> albums;
  String id;
  int v;
  String image;

  factory User.fromJson(Map<String, dynamic> json) => User(
    albums: List<Album>.from(json["albums"].map((x) => Album.fromJson(x))),
    id: json["_id"],
    v: json["__v"],
    image: json["image"]
  );

  Map<String, dynamic> toJson() => {
    "albums": List<dynamic>.from(albums.map((x) => x.toJson())),
    "_id": id,
    "__v": v,
    "image": image
  };
}

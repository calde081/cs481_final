// To parse this JSON data, do
//
//     final album = albumFromJson(jsonString);

import 'dart:convert';
import 'Song.dart';

Album albumFromJson(String str) => Album.fromJson(json.decode(str));

String albumToJson(Album data) => json.encode(data.toJson());

class Album {
  Album({
    this.songList,
    this.id,
    this.title,
    this.artist,
    this.image,
    this.yearProduced,
    this.v,
  });

  List<Song> songList;
  String id;
  String title;
  String artist;
  String image;
  String yearProduced;
  int v;

  factory Album.fromJson(Map<String, dynamic> json) => Album(
    songList: List<Song>.from(json["songList"].map((x) => Song.fromJson(x))),
    id: json["_id"],
    title: json["title"],
    artist: json["artist"],
    image: json["image"],
    yearProduced: json["yearProduced"],
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "songList": List<dynamic>.from(songList.map((x) => x.toJson())),
    "_id": id,
    "title": title,
    "artist": artist,
    "image": image,
    "yearProduced": yearProduced,
    "__v": v,
  };
}

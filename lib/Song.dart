// To parse this JSON data, do
//
//     final song = songFromJson(jsonString);

import 'dart:convert';

Song songFromJson(String str) => Song.fromJson(json.decode(str));

String songToJson(Song data) => json.encode(data.toJson());

class Song {
  Song({
    this.no,
    this.title,
    this.length,
    this.url,
    this.isFav,
    this.album
  });

  String no;
  String title;
  String length;
  String url;
  bool isFav;
  String album;

  factory Song.fromJson(Map<String, dynamic> json) => Song(
    no: json["no"],
    title: json["title"],
    length: json["length"],
    url: json["url"],
    isFav: json["isFav"],
    album: json["album"]
  );

  Map<String, dynamic> toJson() => {
    "no": no,
    "title": title,
    "length": length,
    "url": url,
    "isFav": isFav,
    "album": album
  };
}

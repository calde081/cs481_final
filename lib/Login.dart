import 'package:flutter/material.dart';
import 'main.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String user;
  String password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 38, 50, 56),
          title: Center(child: Text("My Muse", style: TextStyle(fontStyle: FontStyle.italic),)),
        ),
        body:  Container (
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/mymuse2.jpg",),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.srcOver)
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: 250,
                  height: 50,
                  child: TextFormField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.white
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blue
                        ),
                      ),
                      labelText: "Username",
                      labelStyle: TextStyle(color: Colors.white),
                      ),
                    onChanged: (String val) {
                      setState(() {
                        user = val;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 30,),
              // Container(
              //   width: 250,
              //   height: 50,
              //   child: TextFormField(
              //     style: TextStyle(color: Colors.white),
              //     decoration: InputDecoration(
              //         enabledBorder: OutlineInputBorder(
              //           borderSide: BorderSide(
              //             color: Colors.white
              //           ),
              //         ),
              //         focusedBorder: OutlineInputBorder(
              //           borderSide: BorderSide(
              //             color: Colors.blue
              //           ),
              //         ),
              //         labelText: "Password",
              //         labelStyle: TextStyle(color: Colors.white),
              //     ),
              //     obscureText: true,
              //     onChanged: (String val) {
              //       setState(() {
              //         password = val;
              //       });
              //     },
              //   ),
              // ),
              // SizedBox(height: 30,),
              RaisedButton(
                color: Color.fromARGB(255, 38, 50, 56),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
                child: Text("Login", style: TextStyle(color: Colors.white),),
                onPressed: (){
                  username = user;
                  // should check to see if user exists in database and password matches, then only allows user to go forward if password matches
                  // if(username == "mikey" || username == "") {
                    Navigator.pushNamed(context, '/MyBottomNavigationBar');
                  //}
                },
              ),

            ],
          ),
          )
    );
  }
}

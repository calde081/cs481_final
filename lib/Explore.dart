
import 'package:flutter/material.dart';
import 'package:inter_dialo_localization/UserFavoriteList.dart';
import 'package:inter_dialo_localization/main.dart';
import 'Album.dart';
import 'Player.dart';
import 'Song.dart';
import 'Heart.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'User.dart';

class Explore extends StatefulWidget {
  @override
  _ExploreState createState() => _ExploreState();
}

class _ExploreState extends State<Explore>{
  Album album;
  @override
  void initState() {
    super.initState();
    futureUsers = fetchUsers();
  }

  List<User> users =new List<User>();
  Future<List<User>>  futureUsers;
  Future<List<User>> fetchUsers() async {
    final response = await http.get('http://3.21.129.217:3000/api/users?id=' + username);
    if (response.statusCode == 200) {
      List<dynamic> values=new List<dynamic>();
      values = json.decode(response.body);
      if(values.length>0){
        for(int i=0;i<values.length;i++){
          if(values[i]!=null){
            Map<String,dynamic> map=values[i];
            users.add(User.fromJson(map));
            debugPrint('Id-------${map['id']}');
          }
        }
      }
      return users;
    } else {
      throw Exception('Failed to load album');
    }
  }
  Widget GetItemWidgets(List<User> users)
  {
    return new GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 0.0,
        mainAxisSpacing: 2.0,
        children: users.map((user) => _buildTile(user)).toList()
    );
  }
  Widget _buildTile(User user) =>  Card(
    elevation: 0,
    color:Colors.transparent,
    child: Container(
      decoration: BoxDecoration(color: Color.fromARGB(255, 38, 50, 56)),
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => UserFavoriteList(user: user),
              ),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(4),
            width: 150,
            height: 100,
            child: Column(
              children: [
                Stack(
                  children:[
                    Image.asset(
                      'assets/dogs/' + user.image + '.jpg',
                      fit: BoxFit.fill,
                      width: double.maxFinite,
                      height: 170,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ),
    );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 38, 50, 56),
          title: Center(child: Text("Explore")),
        ),
        body:  Container (
          decoration: BoxDecoration(color:Colors.white),
          child:FutureBuilder<List<User>>(
              future: futureUsers,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return GetItemWidgets(users);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              }
          ),
        )
    );
  }
}
import 'package:flutter/material.dart';
import 'package:inter_dialo_localization/main.dart';
import 'User.dart';
import 'Player.dart';
import 'Song.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserFavoriteList extends StatefulWidget {
  final User user;
  UserFavoriteList({Key key, @required this.user}) : super(key: key);
  @override
  _UserFavoriteListState createState() => _UserFavoriteListState();
}

class _UserFavoriteListState extends State<UserFavoriteList>{
  bool shuffle = true;
  User user;
  @override
  void initState() {
    super.initState();
    futureFavSongs = fetchFavSongs();
  }
  List<Song> favSongs =new List<Song>();
  Future<List<Song>>  futureFavSongs;
  Future<List<Song>> fetchFavSongs() async {
    final response = await http.get('http://3.21.129.217:3000/api/fav?id=' + widget.user.id);
    if (response.statusCode == 200) {
      List<dynamic> values=new List<dynamic>();
      values = json.decode(response.body);
      if(values.length>0){
        for(int i=0;i<values.length;i++){
          if(values[i]!=null){
            Map<String,dynamic> map=values[i];
            favSongs.add(Song.fromJson(map));
            debugPrint('Id-------${map['id']}');
          }
        }
      }
      return favSongs;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 38, 50, 56),
          title: Center(child: Text(widget.user.id  + "'s Favorite Songs")),
      ),
      body:  Container (
        decoration: BoxDecoration(color: Colors.white),
          child: FutureBuilder<List<Song>>(
              future: futureFavSongs,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    children: [Container(
                      child: DataTable(columnSpacing: 35, columns: const <DataColumn>[
                        DataColumn(
                          label: Text(
                            'Album',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Title',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                          'Length',
                          style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                          '',
                          style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                  ],
                  rows: favSongs.map(
                  ((Song element) => DataRow(
                  cells: <DataCell>[
                    DataCell(Text(element.album)), //Extracting from Map element the value
                    DataCell(Text(element.title)),
                    DataCell(Text(element.length)),
                    DataCell(ActionChip(label: Icon(Icons.play_arrow, color:Colors.white), onPressed: () {
                      Navigator.push(
                      context,
                      MaterialPageRoute(
                      builder: (context) => Player(song: element, album: null, shuffle: !shuffle, listOfSongs: []),
                      ),
                      );
                    }, backgroundColor: Color.fromARGB(255, 38, 50, 56),))
                  ],
                  )),
                    ).toList()
                    ),
                    ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 25,
                            width: 150,
                            child: RaisedButton(
                              color: Color.fromARGB(255, 38, 50, 56),
                              onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Player(song: null, album: null, shuffle: !shuffle, listOfSongs: favSongs),
                                  ),
                                );
                              },
                              child: Text("Play All",style:TextStyle(color: Colors.white)),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25,
                            width: 150,
                            child: RaisedButton(
                              color: Color.fromARGB(255, 38, 50, 56),
                              // textColor: Colors.black,
                              onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Player(song: null, album: null, shuffle: shuffle, listOfSongs: favSongs),
                                  ),
                                );
                              },
                              child: Text("Shuffle",style:TextStyle(color: Colors.white)),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
                        ],
                      ),
                      Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              }
          )
      )
    );
  }
}
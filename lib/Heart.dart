// Code for the heart animation is from Net Ninja @ https://www.youtube.com/watch?v=k5BDKI7R6ag
// removed some unnecessary code though..about 1/3 of the code he used

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:inter_dialo_localization/PrimaryPage.dart';
import 'package:inter_dialo_localization/main.dart';
import 'dart:convert';

import 'Song.dart';

class Heart extends StatefulWidget {
  @override
  _HeartState createState() => _HeartState();
  final Song song;
  Heart({Key key, @required this.song}) : super(key: key);
}

class _HeartState extends State<Heart> with SingleTickerProviderStateMixin{
  AnimationController _controller;
  Animation _colorAnimation;
  bool isFav = false;

  Future<http.Response> updateFav() {
    return http.post(
      'http://3.21.129.217:3000/api/fav',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'id': username,
        'title': widget.song.album,
        'song': widget.song.title,
        'isFav': this.isFav
      }),
    );
  }

  @override
  void initState() {
    isFav = widget.song.isFav;
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 200),
        vsync: this
    );
    _colorAnimation = ColorTween(begin: Colors.grey[400], end: Colors.red).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context,_) {
          return IconButton(
            icon: Icon(Icons.favorite),
            color: isFav ?  Colors.red : Colors.grey[400],
            iconSize: 25,
            onPressed: () {
              setState(() {
                isFav = !isFav;
              });
              isFav ? _controller.forward() : _controller.reverse();
              updateFav();
            },
          );
        }
    );
  }
}
